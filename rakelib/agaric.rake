# As agaric.rake did not know how to deploy to Drutopia,
# all deployment features have been removed.
# Use drutopia_host to deploy.

require 'date'
require 'rake/clean'

CLEAN.include("#{BUILDDIR}")

ENVIRONMENTS.keys.each do |env|
  release_host = ENVIRONMENTS[env]["host"]
  release_path = ENVIRONMENTS[env]["path"]
  release_backups = ENVIRONMENTS[env]["backups"]
  release_tag = ENVIRONMENTS[env]["tag"]
  drush_path = ENVIRONMENTS[env]["drush"]
  drush_path = drush_path ? drush_path : "drush"

  db_backup_task = "db_backup_#{env}".to_sym
  task db_backup_task do
    file = "#{release_backups}/backup-#{env}-#{DateTime.now}.sql.gz"
    sh "ssh #{release_host} '#{drush_path} -r #{release_path} sql-dump --structure-tables-key=common --gzip > #{file}'"
  end

  db_drop_tables_task = "db_drop_tables_#{env}".to_sym
  task db_drop_tables_task => db_backup_task do
    sh "ssh #{release_host} #{drush_path} -y -r #{release_path} sql-drop"
  end

  file_sync_task = "file_sync_#{env}_to_local".to_sym
  desc "Sync files from #{env} to local environment."
  task file_sync_task do
    sh "rsync -rz --stats --exclude styles --exclude css --exclude js --delete \
      #{release_host}:#{release_path}/sites/default/files/ \
      #{DRUPAL}/sites/default/files/"
  end

  db_sync_task = "db_sync_#{env}_to_local".to_sym
  desc "Sync database from #{env} to local environment."
  task db_sync_task do
    sh "drush -y sql-drop"
    sh "ssh -C #{release_host} #{drush_path} -r #{release_path} \
      sql-dump --structure-tables-key=common | drush sql-cli"
  end

  ENVIRONMENTS.keys.each do |e|
    unless e == env then
      from_host = ENVIRONMENTS[e]["host"]
      from_path = ENVIRONMENTS[e]["path"]
      from_drush = ENVIRONMENTS[e]["drush"]
      from_drush = from_drush ? from_drush : "drush"

      file_sync_task = "file_sync_#{e}_to_#{env}".to_sym
      desc "Sync files from #{e} to #{env} environment."
      task file_sync_task do
        sh "ssh -A #{from_host} rsync -rz --stats --exclude styles \
          --exclude css --exclude js #{from_path}/sites/default/files/ \
          --delete " + (from_host == release_host ? "" : "#{release_host}:") + "#{release_path}/sites/default/files/"
      end
      db_sync_task = "db_sync_#{e}_to_#{env}".to_sym
      desc "Sync database from #{e} to #{env} environment."
      task db_sync_task => db_drop_tables_task do
        sh "ssh -C #{from_host} #{from_drush} -r #{from_path} \
          sql-dump --structure-tables-key=common | \
          ssh -C #{release_host} #{drush_path} -r #{release_path} sql-cli"
      end
    end
  end
end

TAGNAMES.each do |tagname|
  desc "Tag a commit with #{tagname}."
  task "tag_#{tagname}".to_sym do
    sh "git fetch --tags"
    num = `git tag`.scan(Regexp.new(tagname + "-")).size + 1
    sh "git tag -am '#{tagname.upcase} Release #{num}' #{tagname}-#{num}"
    sh "git tag -afm 'Current #{tagname.upcase} Release' #{tagname}"
    sh "git push origin :refs/tags/#{tagname}"
    sh "git push origin --tags"
  end
end

desc "Detect coding standard violations in profile and custom modules."
task :sniff do
  files = ["web/profiles/#{PROFILE}", 'web/sites/all/modules/custom'].join(' ')
  extensions = ['module', 'profile', 'install', 'inc', 'php'].join(',')
  sh "phpcs --extensions=#{extensions} #{files}"
end

namespace :tests do

  desc "Run integration tests."
  task "integration" do
    sh "phpunit --process-isolation --bootstrap=tests/bootstrap.php tests/integration"
  end

end

if defined? PROFILE
  desc "Delete and re-install a site from its installation profile."
  task "site_install" do
    sh "drush -y site-install #{PROFILE}"
  end
end
